<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Title Page-->
    <title>ตรวจสอบสถานะการใช้งาน</title>
    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
    <link href="css/main2.css" rel="stylesheet" media="all">
    <script src='https://cdn.jsdelivr.net/npm/sweetalert2'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>

<script>



function Verified(name, lastname ,Username ,Password) {
	
	Swal.fire(
  'ยืนยันการลงทะเบียนแล้ว',
  'ชื่อผู้ใช้ : ' + Username + ' รหัสผ่าน : ' + Password,
  'success'
)
  
}

function NotVerified(name,lastname) {
  Swal.fire(
  'ยังไม่ได้รับการยืนยัน',
  'คุณ '+ name + "  " + lastname +' ยังไม่สามารถเข้าระบบได้',
  'error'
)
}

function NotFoundData() {
  Swal.fire(
  'ไม่พบข้อมูล',
  'กรุณาตรวจสอบหมายเลขอีกครั้ง',
  'error'
)
}

function ErrorAjax() {
  Swal.fire(
  'เกิดข้อผิดพลาด',
  'เกิดข้อผิดพลาดที่ไม่คาดคิด',
  'error'
)
}

function AjaxUserStatus(){
   $.ajax({
    type: "POST",
    url: "CheckStatusonDB.php",
    dataType:"json",
    data: { identify_idAjax : $("#indentify_id").val()}
  }).done(function( msg ) {
    if(msg[0] === "Verified"){
      Verified(msg[1],msg[2],msg[3],msg[4]);
    }else if(msg[0] === "NotVerified"){
      NotVerified(msg[1],msg[2]);
    }else if(msg[0] === "NotFoundData"){
      NotFoundData();
    }else{
      ErrorAjax();
    }
  });

}
</script>
  <body>
     <div class="page-wrapper bg-gra-02 p-t-130 p-b-100">
        <div class="wrapper wrapper--w680">
           <div class="card card-4">
              <div class="card-body">
                <center>
                 <h2 class="title">ตรวจสอบระบบการใช้งาน</h2>
               </center>
                   <center>
                          <div class="input-group">
                             <label class="label">กรอกรบัตรประจำตัว 13 หลัก / Passport</label>
                             <div class="input-group-icon">
                                <input class="input--style-4" type="text" name="indentify_id" id="indentify_id" required>
                                <i class="fa fa-user input-icon"></i>
                             </div>
                             </div>
                    <div class="p-t-15">
                       <button type="button" class="btn btn--radius-2 btn--blue" onclick="AjaxUserStatus()">ตรวจสอบสถานะ</button>
                     </center>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
              </div>
           </div>
        </div>
     </div>
     <!-- Jquery JS-->
     <script src="vendor/jquery/jquery.min.js"></script>
     <!-- Vendor JS-->
     <script src="vendor/select2/select2.min.js"></script>
     <script src="vendor/datepicker/moment.min.js"></script>
     <script src="vendor/datepicker/daterangepicker.js"></script>
     <!-- Main JS-->
     <script src="js/global.js"></script>
  </body>
  <!-- Jquery JS-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <!-- Vendor JS-->
  <script src="vendor/select2/select2.min.js"></script>
  <script src="vendor/datepicker/moment.min.js"></script>
  <script src="vendor/datepicker/daterangepicker.js"></script>
  <!-- Main JS-->
  <script src="js/global.js"></script>
</body>
</html>
